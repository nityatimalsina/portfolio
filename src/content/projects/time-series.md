---
title: Time-Series Data Analysis on Glacier Decline
publishDate: 2021-03-01
img: /assets/time-series.jpg
img_alt: A graphical representation of glacier decline over time
description: |
  Conducted at Harvard University from Mar 2021 to May 2021, this project analyzed the relationship between glacier decline and global temperature rise through exploratory data analysis, linear regression, and time-series forecasting. Utilizing Meta's Prophet tool, we delved into non-linear trend analysis, offering insights into climate change impacts.

tags:
  - Data Analysis
  - Time-Series Forecasting
  - Climate Science
  - Data Visualization
---

## Methodology

- **Exploratory Data Analysis**: Examined data to understand the relationship between glacier decline and temperature rise.
- **Linear Regression**: Applied linear regression to quantify the association.
- **Time-Series Forecasting**: Utilized Prophet to forecast glacier decline based on historical data.

## Technical Tools

- **Prophet**: Employed for time-series forecasting.
- **Python**: Utilized for data analysis and visualization.

## Insights

- **Correlation**: Identified a significant correlation between global temperature rise and glacier decline.
- **Forecasting**: Provided a forecast of future glacier decline, aiding in climate change awareness.