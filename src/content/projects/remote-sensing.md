---
title: Geospatial Analysis of Urban Sprawl and Methane Emissions
publishDate: 2020-01-01
img: /assets/geospatial-analysis.jpg
img_alt: Geospatial visualization of methane emissions over urban areas
description: |
  This project entailed a meticulous examination of the relationship between urban sprawl and methane emissions, leveraging data from ESRI, Google Earth, NASA, and EPA. Through multilayer geospatial visualizations and predictive analysis, we illustrated methane level increases in Los Angeles from 2000 to 2015, shedding light on urban sprawl's environmental impact.

tags:
  - Remote Sensing
  - Geospatial Analysis
  - Environmental Science
  - Urban Studies
---

## Geospatial Techniques

- **Multi-Layer Visualizations**: Developed multi-layer visualizations to depict methane emissions over time.
- **Image Differentiations**: Utilized image differentiations to analyze urban sprawl.
- **Heatmaps**: Created heatmaps to illustrate methane level increases.

## Technical Proficiency

- **ArcGIS and QGIS**: Employed for geospatial analysis and visualizations.
- **NASA Worldview**: Utilized for satellite imagery analysis.
- **Machine Learning**: Integrated machine learning algorithms for predictive analysis.

## Environmental Insight

- **Urban Sprawl Impact**: Uncovered the environmental implications of urban sprawl on methane emissions.
- **Predictive Analysis**: Forecasted methane emission trends based on urban sprawl metrics.
