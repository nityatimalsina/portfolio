# Nitya Timalsina's Tech Portfolio and Blog

> Bridging traditional insights with innovative technology, focusing on impactful engineering solutions, ethical AI, and community upliftment.

[![Open in GitLab](https://badgen.net/badge/Open%20in/GitLab/FCA121)](https://gitlab.com/nityatimalsina/portfolio)
[![Built with Astro](https://badgen.net/badge/Built%20with/Astro/2EA44F)](https://astro.build)
[![Visit Website](https://badgen.net/badge/Visit/Website/8A2BE2)](https://nityatimalsina.com/)

## Table of Contents

- [Overview](#overview)
- [About Me](#about-me)
- [Features](#features)
- [Technologies Used](#technologies-used)
- [Quick Start](#quick-start)
- [Commands](#commands)
- [Collaboration](#collaboration)

## Overview

This portfolio and blog serve as a platform to showcase my roles and expertise in entrepreneurship, software architecture, and ethical AI.

## About Me

- 🎓 **Educational Background**: Master's in Software Engineering, certificates in Data Science and Cybersecurity from Harvard.
- 🌱 **Entrepreneurial Ventures**: Founder, One Step Projects.
- 💡 **Technical Expertise**: Software architecture, full-stack development, AI/NLP, blockchain, cybersecurity.

## Features

### Project Showcases

- Case studies focusing on community-driven engineering solutions and ethical AI applications.

### Tech Blog

- In-depth articles and actionable guides, exploring the latest tech trends and ethical frameworks.

## Technologies Used

Refer to `package.json` for specific versions:

- **Astro**: For frontend development
- **TypeScript**: For type safety and scalable code
- **rsync**: For file synchronization post-build
- **@astrojs/check**: For build-time validations

## Quick Start

### Installation and Running

```sh
# Clone the repository
git clone https://gitlab.com/your-gitlab-repo-link.git

# Navigate to the project directory
cd your-gitlab-repo-directory

# Install dependencies
npm install

# Run the development server
npm run dev

# Open your browser and navigate to
http://localhost:4321
```

## Commands
All commands are run from the root of the project, from a terminal:

| Command                   | Action                                           |
| :------------------------ | :----------------------------------------------- |
| `npm install`             | Installs dependencies                            |
| `npm run dev`             | Starts local dev server at `localhost:4321`      |
| `npm run build`           | Build your production site to `./dist/`          |
| `npm run preview`         | Preview your build locally, before deploying     |
| `npm run astro ...`       | Run CLI commands like `astro sync`, `astro add`, and `astro check` |
| `npm run astro -- --help` | Get help using the Astro CLI                     |


## Collaboration
Open to consulting opportunities and partnerships that align with my expertise and values.